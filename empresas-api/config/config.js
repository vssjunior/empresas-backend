export default {
    database: 'empresas',
    username: '',
    password: '',
    params: {
        dialect: 'sqlite',
        storage: 'empresas.sqlite',
        define: {
            underscored: true
        }
    }
}