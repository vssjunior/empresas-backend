export default (sequelize, DataType) => {
    const Empresas = sequelize.define('Empresas', {
        id: {
            type: DataType. INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nome: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    });
    return Empresas;
}