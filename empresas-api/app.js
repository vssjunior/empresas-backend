import express from 'express';
import config from 'config';
import datasource from './config/datasource';

const app = express();
app.config = config;
app.datasource = datasource(app);
app.set('port', 7000);
const Empresas = app.datasource.models.Empresas;

app.route('/empresas')
    .get((req, res) => {
        Empresas.findAll({})
            .then(result => res.json(result))
            .catch(err => res.status(412));        
    });

export default app;