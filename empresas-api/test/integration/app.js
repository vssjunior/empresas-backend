describe('Routes Empresas', () => {
    const Empresas = app.datasource.models.Empresas,
    defaultEmpresa = {
        id: 1,
        name: 'Default Empresa'
    };

    beforeEach(done => {
        Empresas
            .destroy({where: {}})
            .then(() => Empresas.create(defaultEmpresa))
            .then(() => {
                done();
            });
    });

    describe('Rout GET /empresas', () => {
        it('should return a list of empresas', done => {
            request
                .get('/empresas')
                .end((err, res) => {
                    expect(res.body[0].nome).to.be.eql(defaultEmpresa.name);
                    expect(res.body[0].id).to.be.eql(defaultEmpresa.id);
                    done(err);
            });
        });
    });
});